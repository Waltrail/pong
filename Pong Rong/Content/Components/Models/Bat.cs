﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace Pong_Rong.Components.Models
{
    public class Bat : Sprite
    {
        public int state;
        public Bat(Texture2D texture) : base(texture)
        {
            Speed = 20f;
        }
        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            if (input == null)
                throw new Exception("Input = null");
            if (state == 1)
            {
                if (Keyboard.GetState().IsKeyDown(input.Left))
                {
                    Velocity.X = -Speed;
                }
                else if (Keyboard.GetState().IsKeyDown(input.Right))
                {
                    Velocity.X = Speed;
                }
            }
            if (state == 2)
            {
                if (Keyboard.GetState().IsKeyDown(input.Up))
                {
                    Velocity.Y = -Speed;
                }
                else if (Keyboard.GetState().IsKeyDown(input.Down))
                {
                    Velocity.Y = Speed;
                }
            }
            Position += Velocity;
            Position.X = MathHelper.Clamp(Position.X, 63, Game1.ScreenWight - _texture.Width - 61);
            Position.Y = MathHelper.Clamp(Position.Y, 115, Game1.ScreenHight - _texture.Height - 32);
            Velocity = Vector2.Zero;
        }
    }
}
