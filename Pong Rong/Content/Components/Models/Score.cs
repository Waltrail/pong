﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Pong_Rong.Components.Models
{
    public class Score
    {
        public int score;
        private SpriteFont _font;
        private Texture2D _texture;
        public Vector2 Position, FontPosition;
        public string LineScore = "";
        public Score(Texture2D texture, SpriteFont font)
        {
            _font = font;
            _texture = texture;            
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_texture, Position, Color.White);
            spriteBatch.DrawString(_font, LineScore, FontPosition, Color.White);
        }
        public void Update()
        {
            LineScore = "Score: " + score.ToString();
        }
    }
}
