﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Pong_Rong.Components.Models
{
    public class Ball : Sprite
    {


        private bool IsPlaying = true;
        public Vector2 scale;
        public Color color;
        public float radius;
        public int state;
        private float _timer = 0f;
        private Vector2? _startPosition = null;
        private float? _startSpeed;
        public Score score;
        public Score score2;
        public int speedIncSpan = 10;

        public Ball(Texture2D _texture)
            : base(_texture)
        {
            Speed = 10f;
        }
        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            if (_startPosition == null)
            {
                _startPosition = Position;
                _startSpeed = Speed;
                Restart();
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                if (!IsPlaying && state == 1)
                {
                    score.score = 0;
                }
                IsPlaying = true;
            }
            if (!IsPlaying)
            {
                return;
            }
            _timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (_timer > speedIncSpan)
            {
                Speed++;
                _timer = 0;
            }
            foreach (var sprite in sprites)
            {
                if (sprite == this)
                {
                    continue;
                }
                if (this.Velocity.X > 0 && this.IsTouchingLeft(sprite))
                {
                    this.Velocity.X = -this.Velocity.X;
                    if (sprite is Bat && state == 1)
                    {
                        score.score++;
                    }
                }
                if (this.Velocity.X < 0 && this.IsTouchingRight(sprite))
                {
                    this.Velocity.X = -this.Velocity.X;
                    if (sprite is Bat && state == 1)
                    {
                        score.score++;
                    }
                }
                if (this.Velocity.Y > 0 && this.IsTouchingTop(sprite))
                {
                    this.Velocity.Y = -this.Velocity.Y;
                    if (sprite is Bat && state == 1)
                    {
                        score.score++;
                    }
                }
                if (this.Velocity.Y < 0 && this.IsTouchingBottom(sprite))
                {
                    this.Velocity.Y = -this.Velocity.Y;
                    if (sprite is Bat && state == 1)
                    {
                        score.score++;
                    }
                }
            }
            if (state == 1)
            {
                if (Position.Y + _texture.Height >= Game1.ScreenHight)
                {
                    Restart();
                }
                if (Position.Y <= 114)
                {
                    Velocity.Y = -Velocity.Y;
                }
                if (Position.X <= 61 || Position.X + _texture.Width >= Game1.ScreenWight - 61)
                {
                    Velocity.X = -Velocity.X;
                }
                Position += Velocity * Speed;
            }
            if(state == 2)
            {
                if (Position.X + _texture.Width >= Game1.ScreenWight)
                {
                    Restart();
                    score.score++;                    
                }
                if(Position.X <= 0)
                {
                    Restart();
                    score2.score++;                    
                }
                if (Position.Y <= 114 || Position.Y + _texture.Height >=Game1.ScreenHight - 32)
                {
                    Velocity.Y = -Velocity.Y;
                }
               
                Position += Velocity * Speed;
            }
        }

        public void Restart()
        {
            var direction = Game1.random.Next(0, 4);
            switch (direction)
            {
                case 0:
                    Velocity = new Vector2(1, 1);
                    break;
                case 1:
                    Velocity = new Vector2(-1, -1);
                    break;
                case 2:
                    Velocity = new Vector2(-1, 1);
                    break;
                case 3:
                    Velocity = new Vector2(1, -1);
                    break;
            }
            Position = new Vector2(Game1.random.Next(330, Game1.ScreenWight - _texture.Width / 2 - 330), Game1.random.Next(260, Game1.ScreenHight - _texture.Height / 2 - 260));
            Speed = (float)_startSpeed;
            _timer = 0;
            IsPlaying = false;
        }
    }
}