﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using Pong_Rong.Components.Controls;

namespace Pong_Rong.Components.States
{
    public class Menustate : State
    {

        private List<Component> _components;
        private Texture2D MenuBack;
        private Texture2D _logo;
        public Menustate(Game1 game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            var Logo = _content.Load<Texture2D>("Sprites/logo");
            var ButtonNewGame_tex = _content.Load<Texture2D>("Sprites/buttons_new_game");
            var ButtonQuit_tex = _content.Load<Texture2D>("Sprites/buttons_quit");
            var Button2Players_tex = _content.Load<Texture2D>("Sprites/bourd_2players");
            var MenuBackground = _content.Load<Texture2D>("Sprites/menu");
            var NewGameButton = new Button(ButtonNewGame_tex)
            {
                position = new Vector2(Game1.ScreenWight / 2 - ButtonNewGame_tex.Width / 2, 546)
            };
            NewGameButton.Click += NewGameButton_Click;
            var QuitButton = new Button(ButtonQuit_tex)
            {
                position = new Vector2(Game1.ScreenWight / 2 - ButtonQuit_tex.Width / 2, 814)
            };
            var MultiPlayersButton = new Button(Button2Players_tex)
            {
                position = new Vector2(Game1.ScreenWight / 2 - ButtonNewGame_tex.Width / 2, 680)
            };
            MultiPlayersButton.Click += MultiPlayersButton_Click;
            QuitButton.Click += QuitButton_Click;
            MenuBack = MenuBackground;
            _logo = Logo;
            _components = new List<Component>()
            {
                NewGameButton,
                QuitButton,
                MultiPlayersButton,
            };
        }

        private void MultiPlayersButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new MultState(_game, _graphicsDevice, _content));
        }

        private void QuitButton_Click(object sender, EventArgs e)
        {
            _game.Exit();
        }

        private void NewGameButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new GameState(_game, _graphicsDevice, _content));
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(MenuBack, new Vector2(0, 0), Color.White);
            foreach (var component in _components)
            {
                component.Draw(gameTime, spriteBatch);
            }
            spriteBatch.Draw(_logo, new Vector2(337,270), Color.White);
            spriteBatch.End();
        }

        public override void Update(GameTime gameTime)
        {
            foreach (var component in _components)
            {
                component.Update(gameTime);
            }
        }
    }
}
