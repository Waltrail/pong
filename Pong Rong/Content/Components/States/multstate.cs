﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Pong_Rong.Components.Models;
using Pong_Rong.Components.Controls;

namespace Pong_Rong.Components.States
{
    public class MultState : State

    {
        private bool IsPaused = false;
        private bool IsRuning = false;
        private bool IsWon = false;
        private Texture2D Pause;
        private List<Component> _components;
        private List<Sprite> _sprites;
        private Texture2D _bourd, Playerwon, Player1_won , Player2_won;
        private Score score;
        private Score score2;       
        public MultState(Game1 game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            var MenuPause = _content.Load<Texture2D>("Sprites/pause");
            var Score1_tex = content.Load<Texture2D>("Sprites/score1");
            var Score2_tex = content.Load<Texture2D>("Sprites/score2");
            var Ball_tex = content.Load<Texture2D>("Sprites/ball");
            var Ground_tex = content.Load<Texture2D>("Sprites/bourd2");
            var Player1_Bat_tex = content.Load<Texture2D>("Sprites/bat1");
            var Player2_Bat_tex = content.Load<Texture2D>("Sprites/bat2");           
            var ButtonQuit_tex = _content.Load<Texture2D>("Sprites/buttons_quit");
            var ButtonContinue_tex = _content.Load<Texture2D>("Sprites/buttons_continue");
            var ButtonMenu_tex = _content.Load<Texture2D>("Sprites/buttons_menu");
            Player1_won = content.Load<Texture2D>("Sprites/gpw");
            Player2_won = content.Load<Texture2D>("Sprites/ppw");
            score = new Score(Score1_tex, content.Load<SpriteFont>("Font/score")) { Position = new Vector2(710,22), FontPosition = new Vector2(720, 32), LineScore = "Space to"};
            score2 = new Score(Score2_tex, content.Load<SpriteFont>("Font/score")) { Position = new Vector2(960,22), FontPosition = new Vector2(970, 32), LineScore = "start!"};
            _bourd = Ground_tex;
            Pause = MenuPause;
            _sprites = new List<Sprite>() {
                new Bat(Player1_Bat_tex)
                {
                    state = 2,
                    Position = new Vector2(Game1.ScreenWight/2-1050-Player1_Bat_tex.Width,Game1.ScreenHight/2),
                    input = new Imput()
                    {
                        Up = Microsoft.Xna.Framework.Input.Keys.W,
                        Down = Microsoft.Xna.Framework.Input.Keys.S
                    }
                },
                new Bat(Player2_Bat_tex)
                {
                    state = 2,
                    Position = new Vector2(Game1.ScreenWight/2+1050,Game1.ScreenHight/2),
                    input = new Imput()
                    {
                        Up = Microsoft.Xna.Framework.Input.Keys.Up,
                        Down = Microsoft.Xna.Framework.Input.Keys.Down
                    }
                },
                new Ball(Ball_tex)
                {
                    Position = new Vector2(Game1.random.Next(330, Game1.ScreenWight - Ball_tex.Width / 2 - 330), Game1.random.Next(260, Game1.ScreenHight - Ball_tex.Height / 2 - 260)),
                    score = score,
                    score2 = score2,
                    state = 2,
                    Speed = 5f,
                    speedIncSpan = 1
                }
            };
            //________________________________________________BUTTONS:
            //Continue
            var ContinueButton = new Button(ButtonContinue_tex)
            {
                position = new Vector2(Game1.ScreenWight / 2 - ButtonQuit_tex.Width / 2, 303)
            };
            ContinueButton.Click += ContinueButton_Click;
            //Menu
            var MenuButton = new Button(ButtonMenu_tex)
            {
                position = new Vector2(Game1.ScreenWight / 2 - ButtonQuit_tex.Width / 2, 433)
            };
            MenuButton.Click += MenuButton_Click;
            //Quit
            var QuitButton = new Button(ButtonQuit_tex)
            {
                position = new Vector2(Game1.ScreenWight / 2 - ButtonQuit_tex.Width / 2, 567)
            };
            QuitButton.Click += QuitButton_Click;

            _components = new List<Component>()
            {
                ContinueButton,
                MenuButton,
                QuitButton,
            };
        }

        private void MenuButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new Menustate(_game, _graphicsDevice, _content));
        }

        private void ContinueButton_Click(object sender, EventArgs e)
        {
            IsPaused = false;
        }

        private void QuitButton_Click(object sender, EventArgs e)
        {
            _game.Exit();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(_bourd, new Vector2(0, 0), Color.White);
            foreach (var sprite in _sprites)
            {
                sprite.Draw(gameTime, spriteBatch);
            }
            score.Draw(spriteBatch);
            score2.Draw(spriteBatch);
            if (IsWon)
            {
                spriteBatch.Draw(Playerwon, new Vector2(Game1.ScreenWight / 2 - Playerwon.Width / 2, Game1.ScreenHight / 2 - Playerwon.Height / 2), Color.White);
            }
            if (IsPaused)
            {
                spriteBatch.Draw(Pause, Vector2.Zero, Color.White);
                foreach (var component in _components)
                {
                    component.Draw(gameTime, spriteBatch);
                }
            }
            spriteBatch.End();
        }

        public override void Update(GameTime gameTime)
        {
            if (IsRuning)
            {
                score.Update(); score2.Update();
            }
            if (score.score >= 8 || score2.score >= 8)
            {
                IsRuning = false;
                Console.WriteLine("Someone won!");
                if (score.score >= 8)
                {
                    Playerwon = Player1_won;
                }
                else
                {
                    Playerwon = Player2_won;
                }
                IsWon = true;
                if (Keyboard.GetState().IsKeyDown(Keys.Space))
                {
                    score.score = 0; score2.score = 0;
                    IsWon = false;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {                
                if (IsPaused)
                    IsPaused = false;               
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                if (!IsPaused)
                    if (IsWon)
                    {
                        
                        score.score = 0; score2.score = 0;                        
                        IsWon = false;
                    }
                    IsPaused = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                IsRuning = true;  
            }
            if (IsPaused)
            {
                IsMouseVisible = true;
                foreach (var component in _components)
                {
                    component.Update(gameTime);
                }
                Console.WriteLine("Paused");
            }
            else
            {
                IsMouseVisible = false;
                foreach (var sprite in _sprites)
                {
                    sprite.Update(gameTime, _sprites);
                    Console.WriteLine("Updating");
                }
            }
        }
    }
}
