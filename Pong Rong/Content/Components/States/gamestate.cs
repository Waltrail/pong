﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Pong_Rong.Components.Models;
using Pong_Rong.Components.Controls;

namespace Pong_Rong.Components.States
{
    public class GameState : State

    {
        private bool IsPaused = false;
        private bool IsRuning = false;
        private Texture2D Pause;
        private List<Component> _components;
        private List<Sprite> _sprites;
        private Texture2D _bourd;
        private Score score;
        public GameState(Game1 game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            var MenuPause = _content.Load<Texture2D>("Sprites/pause");
            var Score_tex = content.Load<Texture2D>("Sprites/score");
            var Ball_tex = content.Load<Texture2D>("Sprites/ball");
            var Ground_tex = content.Load<Texture2D>("Sprites/bourd");
            var Bat_tex = content.Load<Texture2D>("Sprites/bat");
            var Blocker_tex = content.Load<Texture2D>("Sprites/blocker");
            var ButtonQuit_tex = _content.Load<Texture2D>("Sprites/buttons_quit");
            var ButtonContinue_tex = _content.Load<Texture2D>("Sprites/buttons_continue");
            var ButtonMenu_tex = _content.Load<Texture2D>("Sprites/buttons_menu");
            score = new Score(Score_tex, content.Load<SpriteFont>("Font/score")) { Position = new Vector2(62, 22),
            FontPosition = new Vector2(72, 32),
            LineScore = "Space to start"
        };
            _bourd = Ground_tex;
            Pause = MenuPause;
            _sprites = new List<Sprite>() {
                new Bat(Bat_tex)
                {
                    Position = new Vector2(Game1.ScreenWight/2,Game1.ScreenHight-66),
                    state = 1,
                    input = new Imput()
                    {
                        Left = Keys.A,
                        Right = Keys.D
                    }
                },
                new Ball(Ball_tex)
                {
                    Position = new Vector2(Game1.random.Next(330, Game1.ScreenWight - Ball_tex.Width / 2 - 330), Game1.random.Next(260, Game1.ScreenHight - Ball_tex.Height / 2 - 260)),
                    score = score, Speed = 8f,
                    state = 1
                }
            };
            //________________________________________________BUTTONS:
            //Continue
            var ContinueButton = new Button(ButtonContinue_tex)
            {
                position = new Vector2(Game1.ScreenWight / 2 - ButtonQuit_tex.Width / 2, 303)
            };
            ContinueButton.Click += ContinueButton_Click;
            //Menu
            var MenuButton = new Button(ButtonMenu_tex)
            {
                position = new Vector2(Game1.ScreenWight / 2 - ButtonQuit_tex.Width / 2, 433)
            };
            MenuButton.Click += MenuButton_Click;
            //Quit
            var QuitButton = new Button(ButtonQuit_tex)
            {
                position = new Vector2(Game1.ScreenWight / 2 - ButtonQuit_tex.Width / 2, 567)
            };
            QuitButton.Click += QuitButton_Click;

            _components = new List<Component>()
            {
                ContinueButton,
                MenuButton,
                QuitButton,
            };
        }

        private void MenuButton_Click(object sender, EventArgs e)
        {
            _game.ChangeState(new Menustate(_game, _graphicsDevice, _content));
        }

        private void ContinueButton_Click(object sender, EventArgs e)
        {
            IsPaused = false;
        }

        private void QuitButton_Click(object sender, EventArgs e)
        {
            _game.Exit();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            spriteBatch.Draw(_bourd, new Vector2(0, 0), Color.White);
            foreach (var sprite in _sprites)
            {
                sprite.Draw(gameTime, spriteBatch);
            }
            score.Draw(spriteBatch);
            if (IsPaused)
            {
                spriteBatch.Draw(Pause, Vector2.Zero, Color.White);
                foreach (var component in _components)
                {
                    component.Draw(gameTime, spriteBatch);
                }
            }
            spriteBatch.End();
        }

        public override void Update(GameTime gameTime)
        {
            if (IsRuning)
                score.Update();

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                
                if (IsPaused)
                    IsPaused = false;               
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                
                if (!IsPaused)
                    IsPaused = true;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                IsRuning = true;  
            }
            if (IsPaused)
            {
                IsMouseVisible = true;
                foreach (var component in _components)
                {
                    component.Update(gameTime);
                }
                Console.WriteLine("Paused");
            }
            else
            {
                IsMouseVisible = false;
                foreach (var sprite in _sprites)
                {
                    sprite.Update(gameTime, _sprites);
                    Console.WriteLine("Updating");
                }
            }
        }
    }
}
