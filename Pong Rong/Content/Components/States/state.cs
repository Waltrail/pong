﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Pong_Rong;

namespace Pong_Rong.Components
{
    public abstract class State
    {
        protected ContentManager _content;
        protected GraphicsDevice _graphicsDevice;
        protected Game1 _game;
        public bool IsMouseVisible;

        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);       
        public abstract void Update(GameTime gameTime);
        public State(Game1 game, GraphicsDevice graphicsDevice, ContentManager content)
        {
            _game = game; _graphicsDevice = graphicsDevice; _content = content;
        }
    }
}