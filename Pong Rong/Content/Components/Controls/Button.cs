﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Pong_Rong.Components.Controls
{
    public class Button : Component
    {
        private MouseState _currentMouse;
        private bool isHover;
        private MouseState _prevMouse;
        private Texture2D _texture;
        public event EventHandler Click;
        public Color color { get; set; }
        public bool Clicked { get; private set; }
        public Vector2 position { get; set; }
        public Rectangle rectangle
        {
            get
            {
                return new Rectangle((int)position.X, (int)position.Y, _texture.Width, _texture.Height);
            }
        }
        public Button(Texture2D texture)
        {
            _texture = texture;
            color = Color.Gray;
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var _color = Color.White;
            if (isHover)
            {
                _color = Color.Gray;
            }
            spriteBatch.Draw(_texture, rectangle, _color);
        }
        public override void Update(GameTime gameTime)
        {

            _prevMouse = _currentMouse;
            _currentMouse = Mouse.GetState();
            var MouseRectangle = new Rectangle(_currentMouse.X, _currentMouse.Y, 1, 1);
            isHover = false;
            if (MouseRectangle.Intersects(rectangle))
            {
                isHover = true;
                if (_currentMouse.LeftButton == ButtonState.Released && _prevMouse.LeftButton == ButtonState.Pressed)
                {
                    Click?.Invoke(this, new EventArgs());
                }
            }
        }
    }
}