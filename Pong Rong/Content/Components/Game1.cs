﻿using Pong_Rong.Components.States;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Pong_Rong.Components;
using System;
using Microsoft.Xna.Framework.Audio;

namespace Pong_Rong
{
    public class Game1 : Game
    {        
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        //SoundEffectInstance start;
        SoundEffect loop;      

        public static int ScreenHight;
        public static int ScreenWight;
        public static Random random;      
        
        private State _currentState;
        private State _nextState;

        public void ChangeState(State state)
        {
            _nextState = state;
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //graphics.SynchronizeWithVerticalRetrace = false;
            //TargetElapsedTime = new TimeSpan(0, 0, 0, 0, 10);
            //TimeSpan.FromSeconds(1.0f / 144.0f);
            //IsFixedTimeStep = false;
        }

        protected override void Initialize()
        {
            graphics.PreferredBackBufferHeight = 1080;
            graphics.PreferredBackBufferWidth = 1920;
            graphics.HardwareModeSwitch = false;
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            IsMouseVisible = true;
            //IsFixedTimeStep = false;           
           
            ScreenHight = graphics.PreferredBackBufferHeight;
            ScreenWight = graphics.PreferredBackBufferWidth;
            random = new Random();            
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            _currentState = new Menustate(this, graphics.GraphicsDevice, Content);
            //start = Content.Load<SoundEffectInstance>("Sounds/start");
            loop = Content.Load<SoundEffect>("Sounds/strings");
            var song = loop.CreateInstance();
            song.IsLooped = true;
            song.Volume = 0.2f;
            song.Play();
            //player.PlayLooping();
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
           // if(start.State == SoundState.Stopped)
           // {
           //loop.Play();
           // }
            if (_nextState != null)
            {
                _currentState = _nextState;
                _nextState = null;
            }
            if (_currentState is Menustate)
            {
                IsMouseVisible = true;
            }
            else
            {
                if (_currentState.IsMouseVisible)
                    IsMouseVisible = true;
                else
                    IsMouseVisible = false;
            }
            _currentState.Update(gameTime);            
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.LawnGreen);
            _currentState.Draw(gameTime, spriteBatch);
            base.Draw(gameTime);
        }
    }
}
